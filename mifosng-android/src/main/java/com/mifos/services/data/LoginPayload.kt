package com.mifos.services.data

data class LoginPayload (

    var username: String? = null,

    var password: String? = null
)